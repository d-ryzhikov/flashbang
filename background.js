chrome.runtime.onInstalled.addListener(function (details) {
    if (details.reason === 'update') {
        chrome.storage.local.set({
            interval: 2000
        })
    }
});

chrome.runtime.onMessage.addListener(
    function (message, sender, sendResponse) {
        if (message.text === 'getTabId') {
            sendResponse({tabId: sender.tab.id});
        }
    }
);
