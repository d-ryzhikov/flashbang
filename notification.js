var CONTAINER_ID = 'flashbang-notification-container';
var NOTIFICATION_ID = 'flashbang-notification';
var NOTIFICATION_DISPLAY = 'inline-block';

function onAnimationEnd(event) {
    event.target.style.animationPlayState = 'paused';
    event.target.style.display = 'none';
}

function startTimer(timerId, interval, restartAnimation) {
    var newTimer = {};
    newTimer[timerId] = setInterval(restartAnimation, interval);
    newTimer[timerId + 'isActive'] = true;
    chrome.storage.local.set(newTimer);
}

function stopTimer(timerId, intervalId) {
    clearInterval(intervalId);
    var stoppedTimer = {};
    stoppedTimer[timerId] = null;
    chrome.storage.local.set(stoppedTimer);
}

function onToggleTimer(timerId, restartAnimation) {
    return function (message) {
        if (message.text === timerId + 'toggle')
            chrome.storage.local.get(
                ['interval', timerId],
                function (items) {
                    if (items[timerId]) {
                        stopTimer(timerId, items[timerId]);
                    } else {
                        startTimer(
                            timerId,
                            items.interval,
                            restartAnimation
                        );
                    }
                }
            );
    };
}

function onStorageChange(timerId, restartAnimation) {
    return function (changes, areaName) {
        if (areaName === 'local' && 'interval' in changes) {
            chrome.storage.local.get(
                timerId,
                function (items) {
                    if (items[timerId]) {
                        clearInterval(items[timerId]);
                        startTimer(
                            timerId,
                            changes.interval.newValue,
                            restartAnimation
                        );
                    }
                }
            )
        }
    }
}

function main(response) {
    var TIMER_ID = getTimerId(response.tabId);
    var request = new XMLHttpRequest();

    request.open('GET', chrome.runtime.getURL('notification.html'), true);
    request.onreadystatechange = function() {
        if (this.readyState === 4) {
            var container = document.createElement('div');
            container.id = CONTAINER_ID;
            container.innerHTML = this.responseText;
            document.body.appendChild(container);

            var notification = document.getElementById(NOTIFICATION_ID);
            notification.addEventListener('animationend', onAnimationEnd);

            var restartAnimation = function () {
                var currentPlayState = notification.style.animationPlayState;
                notification.style.display = NOTIFICATION_DISPLAY;
                if (currentPlayState !== 'running')
                    notification.style.animationPlayState = 'running';
            };

            chrome.storage.local.get(
                'interval',
                function (items) {
                    startTimer(TIMER_ID, items.interval, restartAnimation);
                }
            );
            chrome.storage.onChanged.addListener(
                onStorageChange(TIMER_ID, restartAnimation)
            );
            chrome.runtime.onMessage.addListener(
                onToggleTimer(TIMER_ID, restartAnimation)
            )
        }
    };
    request.send();
}

chrome.runtime.sendMessage({text: 'getTabId'}, main);
