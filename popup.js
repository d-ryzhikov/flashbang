function main(tabs) {
    var TAB_ID = tabs[0].id;
    var TIMER_ID = getTimerId(TAB_ID);

    chrome.storage.local.get(
        ['interval', TIMER_ID],
        function (items) {
            var intervalInput = document.getElementById(
                'interval-input'
            );
            intervalInput.value = items.interval;

            var notificationToggler = document.getElementById(
                'notification-toggler'
            );
            notificationToggler.checked = !!(items[TIMER_ID]);
            notificationToggler.addEventListener(
                'click',
                function () {
                    chrome.tabs.sendMessage(
                        TAB_ID,
                        {text: TIMER_ID+'toggle'}
                    );
                }
            );

            var form = document.getElementById('interval-form');
            form.addEventListener(
                'submit',
                function (e) {
                    e.preventDefault();
                    var value = Number(intervalInput.value);
                    if (!isNaN(value))
                        chrome.storage.local.set({interval: value});
                }
            );
        }
    );
}

document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.query(
        {active: true, currentWindow: true},
        main
    );
});
